$.extend(true, $.fn.dataTable.defaults, {
    "processiong": true,
    "serverSide": true,
    "responsive": true,
    "oLanguage": {
        "sEmptyTable":   "Tidak ada data yang tersedia pada tabel ini",
        "sProcessing":   "Sedang memproses...",
        "sLengthMenu":   "Tampilkan _MENU_ entri",
        "sZeroRecords":  "Tidak ditemukan data yang sesuai",
        "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix":  "",
        "sSearch":       "",
        "sSearchPlaceholder": "Cari...",
        "sUrl":          "",
        "oPaginate": {
            "sFirst":    "Pertama",
            "sPrevious": "Sebelumnya",
            "sNext":     "Selanjutnya",
            "sLast":     "Terakhir"
        }
    }
});

function Datatable(el) {
    this.el = el;

    this.inittialize = function() {
        var elOptions = Object.assign({}, $(this.el).data());

        // url required
        if ('url' in elOptions == false) {
            return;
        }

        var options = {
            "columnDefs": [],
            "initComplete": function(settings, json) {
                $(this).siblings('.dataTables_length').find('select').
                select2({ minimumResultsForSearch: Infinity });
            },
            "ajax": {
                "url": elOptions.url,
                "method": "POST",
            }
        };

        options['fnRowCallback'] = function(row, data, iDisplayIndex) {
            if ('columnIndex' in elOptions && elOptions.columnIndex != null) {
                var columnIndex = elOptions.columnIndex;
                var info = this.api().page.info();
                var page = info.page;
                var length = info.length;
                var index = (page * length + (iDisplayIndex +1));
                $('td:eq(' + columnIndex + ')', row).html(index);
            }
        };

        options['drawCallback'] = function(settings) {
            var self = this;
            if ($.fn.editableform) {
                $('.editable', self.api().table().body()).editable({
                    error: function(response, newValue) {
                        if(response.status === 500) {
                            return 'insert number!';
                        } else {
                            return response.responseText;
                        }
                    }
                }).off('hidden').on('hidden', function(e, reason) {
                    if(reason === 'save') {
                        $(this).closest('td span').attr('data-order', $(this).text());
                        self.api().ajax.reload()
                    }
                });
            }
        }

        if ('ajaxData' in elOptions) {
            options['ajax']['data'] = elOptions.ajaxData;
        }

        if ('select' in elOptions) {
            options['select'] = {
                "style": "multi" 
            };
        }

        if ('orderCol' in elOptions) {
            options.columnDefs.push({
                targets: [elOptions.orderCol],
                render: function(data, type, row) {
                    var span = jQuery('<span/>');

                    if (typeof data != 'object') {
                        return "Order not compatibel";
                    }

                    var link = jQuery("<a>" + data.id + "</a>");
                    link.attr(data.options);
                    link.attr("href", "javascript::");
                    link.attr("data-emptytext", "Kosong");
                    link.attr("data-type", "text");
                    link.attr("data-mode", "inline");
                    link.addClass("editable");

                    span.attr("data-order", data.id);
                    span.append(link);

                    if (type == "display") {
                        return span.html();
                    }

                    return null;
                }
            });
        }

        if ('columnOptions' in elOptions) {
            if (Array.isArray(elOptions.columnOptions)) {
                options.columnDefs = options.columnDefs.concat(elOptions.columnOptions);
            }

            delete elOptions.columnOptions;
        }

        if ('actionCol' in elOptions) {
            options.columnDefs.push({
                targets: [elOptions.actionCol],
                className: 'text-center',
                orderable: false,
                render: function(data, type, row) {
                    var links = jQuery('<div/>');

                    if (data.length == 0 || typeof data != 'object') {
                        return "Action not compatible";
                    }

                    for (var i=0; i<data.length; i++) {
                        var btnData = data[i];
                        var link = jQuery("<a>" + btnData.label + "</a>");
                        link.attr("href", btnData.url);
                        link.addClass("btn " + btnData.class);

                        if (btnData.options !== undefined) {
                            link.attr(btnData.options);
                        }

                        links.append(link);
                        links.append(" ");
                    }

                    if (type == "display") {
                        return links.html();
                    }

                    return null;
                }
            });
        }

        if ('columnFoot' in elOptions && $(this.el).find('tfoot')) {
            options['footerCallback'] = function(row, data, start, end, display) {
                var _options = Object.assign({}, $(this[0]).data());
                var api = this.api(), data;
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                    i : 0;
                };

                try {
                    _options['columnFoot'].forEach(k => {
                        var total = api.column(k).data().reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);
                        
                        var footElement = $(this[0]).find(`tfoot th:eq(${k})`);
                        var render = eval(footElement.data('render'));
                        total = render(total);
                        
                        $(api.column(k).footer()).html(total);
                    });
                } catch(err) {}
            }
        }

        this.options = options;
        return this;
    };

    this.render = function() {
        var self = this;
        var oTable = $(this.el).DataTable(this.options);

        var filterForm = $(this.el).data('filterForm')
        if (filterForm) {
            $(filterForm).on('submit', function(e) {
                e.preventDefault();
                
                var formData = $(this).serializeArray();
                var qs = [];
                formData.forEach(fd => {
                    fd.value != 'all' ? qs.push(`${fd.name}=${fd.value}`) : false;
                });
                
                var glue = self.options.ajax.url.includes('?') ? '&' : '?';
                var url = self.options.ajax.url;
                url += qs.length > 0 ? glue + qs.join('&') : '';
                oTable.ajax.url(url).load();
            });
        }
    }
}

$(document).ready(function() {
    $('.datatable-class').each(function() {
        var dt = new Datatable(this);
            dt.inittialize();
            dt.render();
    });
});
