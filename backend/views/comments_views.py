from django.shortcuts import render, get_object_or_404, redirect
from ..models import Comments
from ..forms import CommentsForms
from django.contrib.auth.decorators import login_required

from django.contrib import messages

from rest_framework import viewsets
from ..serializers import CommentSerializer

from django.conf import settings

class CommentsList(viewsets.ModelViewSet):
    queryset = Comments.objects.all().order_by('email')
    serializer_class = CommentSerializer

    def get_options(self):
        return "options", {
            "comment": [{'label': obj.email, 'value': obj.pk} for obj in Comments.objects.all()],            
        }

    class Meta:
        datatables_extra_json = ('get_options', )

@login_required(login_url=settings.LOGIN_URL)
def index(request):
    # form = CommentsForms(request.POST, request.FILES or None)
    # if request.method == 'POST':
    #     if request.POST and form.is_valid():
    #         ekskul = form.save(commit=False)
    #         ekskul.save()            
    #         messages.success(request, 'Data Telah Tersimpan')
    #         return redirect('/profile/')  

    #         form = CommentsForms()
    #     else:
    #         messages.warning(request, 'Data Gagal Disimpan')
    #         return redirect('backend_profile_create') 
    context = {
        'title' : 'Comments',
        'comments_status' : 'active',
        'body' : 'theme-blue',
    }
    return render(request, 'admin/comments/list.html', context)

def index_profile(request):    
    context = {
        'title' : 'Comments',
        'comments_status' : 'active',
        'body' : 'theme-blue',
    }
    return render(request, 'admin/comments/list.html', context)

@login_required(login_url=settings.LOGIN_URL)
def delete(request, pk):
    comment = get_object_or_404(Comments, pk=pk)
    comment.delete()
    messages.success(request, 'Data Berhasil Dihapus')

    return redirect('/comments/')