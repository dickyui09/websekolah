from django.shortcuts import render, redirect, HttpResponseRedirect
from django.contrib.auth import authenticate, logout, login
from django.contrib import messages

from ..models import Login
from ..forms import LoginForms
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required


def login_view(request):   
    form = LoginForms(request.POST or None)
    if request.method == 'POST':
        if request.POST and form.is_valid():
            username = request.POST.get('username','')
            password = request.POST.get('password','')
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    request.session.set_expiry(1000000) #sets the exp. value of the session 
                    login(request, user) 
                    return redirect('/dashboard/')

        else:
            messages.error(request, 'Username Atau Password Anda Salah')
            return redirect('/admin/')     

    context = {
        'comp' : False,
        'body' : 'signup-page',
        'class' : 'signup-box',
        'title' : 'Login',
        'title1' : 'Login',
        'title2' : 'Admin',
        'description' : 'Login Admin System Information',
        'form' : form,
    }
    return render(request, 'admin/auth/login.html', context)

@login_required
def logout_view(request):    
    logout(request)
    return redirect('/')
