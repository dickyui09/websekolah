from django.shortcuts import render, redirect,  reverse, get_object_or_404
from ..forms import EkskulForms
from ..models import Ekskul, Gallery, Profile

from django.contrib import messages

from django.contrib.auth.decorators import login_required
from django.utils.html import escape
# from django.contrib.auth.decorators import login_required
from rest_framework import viewsets
from ..serializers import EkskulSerializer

from django.conf import settings


class EkskulList(viewsets.ModelViewSet):
    queryset = Ekskul.objects.all().order_by('name')
    serializer_class = EkskulSerializer

    # def get(self, request, pk, *args, **kwargs):
    #     ekskuls = Ekskul.objects.get(pk=pk)
    #     serializer = UserContactListSerializer(ekskuls)
    #     return Response(serializer.data)

    def get_options(self):
        return "options", {
            "ekskul": [{'label': obj.name, 'value': obj.pk} for obj in Ekskul.objects.all()],            
        }

    class Meta:
        datatables_extra_json = ('get_options', )

@login_required(login_url=settings.LOGIN_URL)
def index(request):  
    ekskuls = Ekskul.objects.all()
    data = {}
    data['object_list'] = ekskuls

    context = {
        'title' : 'Ekstrakurikuler',
        'ekskul_status' : 'active',
        'category_status' : 'active',
        'body' : 'theme-blue',   
        'c_icon' : 'material-icons md-48 md-dark md-inactive',
        't_icon' : 'widgets', 
        'datatables' : 'fasilitas',  
        "data" : data        
    }
    return render(request, 'admin/Category/ekskul/list.html', context)

@login_required(login_url=settings.LOGIN_URL)
def create(request):
    form = EkskulForms(request.POST, request.FILES or None)
    if request.method == 'POST':
        if request.POST and form.is_valid():
            ekskul = form.save(commit=False)
            ekskul.save()            
            messages.success(request, 'Data Telah Tersimpan')
            return redirect('/ekskul/')  

            form = EkskulForms()
        else:
            messages.warning(request, 'Data Gagal Disimpan')
            return redirect('/ekskul/create/')             

    context = {
        'title' : 'Tambah Ekstrakurikuler',        
        'ekskul_status' : 'active',
        'category_status' : 'active',
        'body' : 'theme-blue',     
        'form': form,     
    }
    return render(request, 'admin/Category/ekskul/create.html', context)

@login_required(login_url=settings.LOGIN_URL)
def edit(request, pk):        
    instance = get_object_or_404(Ekskul, pk=pk)
    form = EkskulForms(request.POST or None, request.FILES or None,  instance=instance)
    if request.method == 'POST':        
        if request.POST and form.is_valid():
            form.save() 
            messages.success(request, 'Data Berhasil Diubah')
            return redirect('/ekskul/')
        else:
            form = EkskulForms(instance=instance)
            messages.warning(request, 'Data Gagal Diubah')

    context = {
        'title' : 'Edit Ekstrakurikuler',        
        'ekskul_status' : 'active',
        'category_status' : 'active',
        'body' : 'theme-blue',     
        'form': form,     
    }

    return render(request, 'admin/Category/ekskul/create.html', context)

@login_required(login_url=settings.LOGIN_URL)
def delete(request, pk):
    ekskul = get_object_or_404(Ekskul, pk=pk)
    ekskul.delete()
    messages.success(request, 'Data Berhasil Dihapus')

    return redirect('/ekskul/')


def index_profile(request):    
    ekskul = Ekskul.objects.all()
    gallery = Gallery.objects.all()
    profile = Profile.objects.all()

    context = {
        'title' : 'MI KH MOHAMMAD SAID',  
        'ekskuls' : ekskul,
        'gallerys': gallery,
        'profiles': profile,
        'navbar' : False,

    }
    return render(request, 'user/ekskul/index.html', context) 

