from django.shortcuts import render, redirect, get_object_or_404
from ..forms import EventsForms, CommentsForms
from ..models import Events, Comments, Profile

from django.contrib import messages
from django.contrib.auth.decorators import login_required

from django.utils.html import escape
# from django.contrib.auth.decorators import login_required
from rest_framework import viewsets
from ..serializers import EventsSerializer

from django.conf import settings

class EventsList(viewsets.ModelViewSet):
    queryset = Events.objects.all().order_by('judul')
    serializer_class = EventsSerializer

    def get_options(self):
        return "options", {
            "events": [{'label': obj.judul, 'value': obj.pk} for obj in Events.objects.all()],            
        }

    class Meta:
        datatables_extra_json = ('get_options', )

@login_required(login_url=settings.LOGIN_URL)
def index(request):
    context = {
        'title' : 'Events',
        'events_status' : 'active',
        'news_status' : 'active',
        'body' : 'theme-blue',
    }
    return render(request, 'admin/news/events/list.html', context)

@login_required(login_url=settings.LOGIN_URL)
def create(request):
    form = EventsForms(request.POST, request.FILES or None)
    if request.method == 'POST':
        if request.POST and form.is_valid():
            events = form.save(commit=False)
            events.save()            
            messages.success(request, 'Data Telah Tersimpan')
            return redirect('/events/')  

            form = EventsForms()
        else:
            messages.warning(request, 'Data Gagal Disimpan')
            return redirect('backend_events_create')             

    context = {
        'title' : 'Tambah Events',        
        'events_status' : 'active',
        'news_status' : 'active',
        'body' : 'theme-blue',     
        'form': form,     
    }
    return render(request, 'admin/news/events/create.html', context)

def detail_events(request, pk):    
    post = get_object_or_404(Events, pk=pk)
    profile = Profile.objects.all()
    form = CommentsForms(request.POST  or None)
    if request.method == 'POST':
        if request.POST and form.is_valid():
            comment = form.save(commit=False)
            comment.save()            

            form = CommentsForms()        
    context = {
        'title' : 'MI KH MOHAMMAD SAID',
        'post' : post,     
        'form': form,   
        'profiles' : profile,
        'navbar' : False,
    }
    return render(request, 'user/berita/index.html', context) 

@login_required(login_url=settings.LOGIN_URL)
def edit(request, pk):        
    instance = get_object_or_404(Events, pk=pk)
    form = EventsForms(request.POST or None, request.FILES or None,  instance=instance)
    if request.method == 'POST':        
        if request.POST and form.is_valid():
            form.save()
            messages.success(request, 'Data Berhasil Diubah')
            return redirect('/events/')
        else:
            form = EventsForms(instance=instance)
            messages.warning(request, 'Data Gagal Diubah')

    context = {
        'title' : 'Edit Events',        
        'events_status' : 'active',
        'news_status' : 'active',
        'body' : 'theme-blue',     
        'form': form,     
    }

    return render(request, 'admin/news/events/create.html', context)    

@login_required(login_url=settings.LOGIN_URL)
def delete(request, pk):
    events = get_object_or_404(Events, pk=pk)
    events.delete()
    messages.success(request, 'Data Berhasil Dihapus')

    return redirect('/events/')



