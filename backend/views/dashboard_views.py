from django.shortcuts import render, redirect
from ..models import Ekskul, Fasilitas, Gallery, Events, Comments, Profile
from django.contrib.auth.decorators import login_required
from django.db.models import Sum, Count
from django.conf import settings
from ..views import auth_views

@login_required()
def index(request):    
    ekskul = Ekskul.objects.all().count()
    events = Events.objects.all().count()
    comments = Comments.objects.all().count()
    context = {
        'title' : 'Dashboard',
        'dashboard_status' : 'active',        
        'body' : 'theme-blue',  
        'ekskul' : ekskul,      
        'events' : events,      
        'comments' : comments,      
    }
    return render(request, 'admin/dashboard/index.html', context)

def index_profile(request):
    gallery = Gallery.objects.all()
    events = Events.objects.all()
    profile = Profile.objects.all()
    context = {
        'title' : 'MI KH MOHAMMAD SAID',           
        # 'ekskuls' : ekskul,
        # 'fasilitass' : fasilitas,
        'gallerys' : gallery,
        'eventss' : events,
        'profiles' : profile,
        'navbar' : True,
    }
    return render(request, 'user/beranda/index.html', context)
