from django.shortcuts import render, redirect, get_object_or_404
from ..models import Profile
from ..forms import ProfileForms
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from django.utils.html import escape
# from django.contrib.auth.decorators import login_required
from rest_framework import viewsets
from ..serializers import ProfileSerializer

from django.conf import settings

class ProfileList(viewsets.ModelViewSet):
    queryset = Profile.objects.all().order_by('name')
    serializer_class = ProfileSerializer

    def get_options(self):
        return "options", {
            "ekskul": [{'label': obj.name, 'value': obj.pk} for obj in Profile.objects.all()],            
        }

    class Meta:
        datatables_extra_json = ('get_options', )

@login_required(login_url=settings.LOGIN_URL)
def index(request):
    context = {
        'title' : 'Profile',
        'profile_status' : 'active',
        'news_status' : 'active',
        'body' : 'theme-blue',
    }
    return render(request, 'admin/news/profile/list.html', context)

@login_required(login_url=settings.LOGIN_URL)
def create(request):    
    form = ProfileForms(request.POST, request.FILES or None or None)
    if request.method == 'POST':
        if request.POST and form.is_valid():
            ekskul = form.save(commit=False)
            ekskul.save()            
            messages.success(request, 'Data Telah Tersimpan')
            return redirect('/profile/')  

            form = ProfileForms()
        else:
            messages.warning(request, 'Data Gagal Disimpan')
            return redirect('backend_profile_create')             

    context = {
        'title' : 'Tambah Profile',        
        'profile_status' : 'active',
        'news_status' : 'active',
        'body' : 'theme-blue',     
        'form': form,     
    }
    return render(request, 'admin/news/profile/create.html', context)

@login_required(login_url=settings.LOGIN_URL)
def edit(request, pk):        
    instance = get_object_or_404(Profile, pk=pk)
    form = ProfileForms(request.POST or None, request.FILES or None, instance=instance)
    if request.method == 'POST':        
        if request.POST and form.is_valid():
            form.save()
            messages.success(request, 'Data Berhasil Diubah')
            return redirect('/profile/')
        else:
            form = ProfileForms(instance=instance)
            messages.warning(request, 'Data Gagal Diubah')

    context = {
        'title' : 'Edit Profile',        
        'ekskul_status' : 'active',
        'category_status' : 'active',
        'body' : 'theme-blue',     
        'form': form,     
    }

    return render(request, 'admin/news/profile/create.html', context)    

# @login_required(login_url=settings.LOGIN_URL)
# def delete(request, pk):
#     profile = get_object_or_404(Profile, pk=pk)
#     profile.delete()
#     messages.success(request, 'Data Berhasil Dihapus')

#     return redirect('/profile/')
