from django.shortcuts import render, redirect, reverse, get_object_or_404  
from ..forms import FasilitasForms
from ..models import Fasilitas, Gallery, Profile

from django.contrib import messages

from rest_framework import viewsets
from ..serializers import FasilitasSerializer

from django.contrib.auth.decorators import login_required

from django.conf import settings

class FasilitasList(viewsets.ModelViewSet):
    queryset = Fasilitas.objects.all().order_by('name')
    serializer_class = FasilitasSerializer

    def get_options(self):
        return "options", {
            "fasilitas": [{'label': obj.name, 'value': obj.pk} for obj in Fasilitas.objects.all()],            
        }

    class Meta:
        datatables_extra_json = ('get_options', )

@login_required(login_url=settings.LOGIN_URL)
def index(request):

    context = {
        'title' : 'Fasilitas',
        'fasilitas_status' : 'active',
        'category_status' : 'active',
        'body' : 'theme-blue',        
        'datatables' : 'fasilitas',
    }
    return render(request, 'admin/Category/fasilitas/list.html', context)

@login_required(login_url=settings.LOGIN_URL)
def create(request):
    form = FasilitasForms(request.POST, request.FILES or None)
    if request.method == 'POST':
        if request.POST and form.is_valid():
            fasilitas = form.save(commit=False)
            fasilitas.save()            
            messages.success(request, 'Data Telah Tersimpan')
            return redirect('/fasilitas/')  

            form = FasilitasForms()
        else:
            messages.warning(request, 'Data Gagal Disimpan')
            return redirect('/fasilitas/create/')  
    context = {
        'title' : 'Tambah Fasilitas',
        'fasilitas_status' : 'active',
        'category_status' : 'active',
        'body' : 'theme-blue',
        'form' : form,

    }
    return render(request, 'admin/Category/fasilitas/create.html', context)

def index_profile(request):
    gallery = Gallery.objects.all()
    fasilitas = Fasilitas.objects.all()
    profile = Profile.objects.all()
    context = {
        'title' : 'MI KH MOHAMMAD SAID',  
        'fasilitass' : fasilitas,
        'gallerys': gallery,
        'profiles': profile,
        'navbar' : False,

    }
    return render(request, 'user/fasilitas/index.html', context) 

@login_required(login_url=settings.LOGIN_URL)
def edit(request, pk):        
    instance = get_object_or_404(Fasilitas, pk=pk)
    form = FasilitasForms(request.POST or None, request.FILES or None,  instance=instance)
    if request.method == 'POST':        
        if request.POST and form.is_valid():
            form.save()
            messages.success(request, 'Data Berhasil Diubah')
            return redirect('/fasilitas/')
        else:
            form = FasilitasForms(instance=instance)
            messages.warning(request, 'Data Gagal Diubah')

    context = {
        'title' : 'Edit Fasilitas',        
        'fasilitas_status' : 'active',
        'category_status' : 'active',
        'body' : 'theme-blue',     
        'form': form,     
    }

    return render(request, 'admin/Category/fasilitas/create.html', context)    

@login_required(login_url=settings.LOGIN_URL)
def delete(request, pk):
    fasilitas = get_object_or_404(Fasilitas, pk=pk)
    fasilitas.delete()
    messages.success(request, 'Data Berhasil Dihapus')

    return redirect('/fasilitas/')