from django.shortcuts import render, redirect, HttpResponseRedirect, get_object_or_404
from ..forms import GalleryForms
from ..models import Gallery, Comments, Ekskul, Events, Fasilitas, Prestasi, Profile

from django.contrib import messages

from django.utils.html import escape
# from django.contrib.auth.decorators import login_required
from rest_framework import viewsets
from ..serializers import GallerySerializer

from django.contrib.auth.decorators import login_required

from django.conf import settings

class GalleryList(viewsets.ModelViewSet):
    queryset = Gallery.objects.all().order_by('deskripsi')
    serializer_class = GallerySerializer

    def get_options(self):
        return "options", {
            "gallery": [{'label': obj.deskripsi, 'value': obj.pk} for obj in Gallery.objects.all()],            
        }

    class Meta:
        datatables_extra_json = ('get_options', )

@login_required(login_url=settings.LOGIN_URL)
def index(request):    
    gallery = Gallery.objects.all()
    comments = Comments.objects.all()
    ekskul = Ekskul.objects.all()
    events = Events.objects.all()
    fasilitas = Fasilitas.objects.all()
    prestasi = Prestasi.objects.all()
    profile = Profile.objects.all()

    # if request.method == 'POST':
    #     if request.POST and form.is_valid():
    #         ekskul = form.save(commit=False)
    #         ekskul.save()            
    #         messages.success(request, 'Data Telah Tersimpan')
    #         return HttpResponseRedirect("/gallery/")

    #         form = GalleryForms()
    #     else:
    #         messages.warning(request, 'Data Gagal Disimpan')
    #         return redirect('/gallery/')   
    context = {
        'title' : 'Gallery',
        'gallery_status' : 'active',     
        'body' : 'theme-blue',   
        # 'form' : form,
        'gallerys' : gallery, 
        'ekskulss' : ekskul,
    }
    return render(request, 'admin/gallery/list.html', context)

@login_required(login_url=settings.LOGIN_URL)
def create(request):    
    form = GalleryForms(request.POST, request.FILES or None)
    if request.method == 'POST':
        if request.POST and form.is_valid():
            gallery = form.save(commit=False)
            gallery.save()            
            messages.success(request, 'Data Telah Tersimpan')
            return redirect("/gallery/")

            form = GalleryForms()
        else:
            messages.warning(request, 'Data Gagal Disimpan')
            return redirect('/gallery/')   
    context = {
        'title' : 'Tambah Gallery',
        'gallery_status' : 'active',     
        'body' : 'theme-blue',   
        'form' : form,
    }
    return render(request, 'admin/gallery/create.html', context)

@login_required(login_url=settings.LOGIN_URL)
def edit(request, pk):        
    instance = get_object_or_404(Gallery, pk=pk)
    form = GalleryForms(request.POST or None, request.FILES or None,  instance=instance)
    if request.method == 'POST':        
        if request.POST and form.is_valid():
            form.save()
            messages.success(request, 'Data Berhasil Diubah')
            return redirect('/gallery/')
        else:
            form = GalleryForms(instance=instance)
            messages.warning(request, 'Data Gagal Diubah')

    context = {
        'title' : 'Edit Gallery',        
        'ekskul_status' : 'active',
        'category_status' : 'active',
        'body' : 'theme-blue',     
        'form': form,     
    }

    return render(request, 'admin/gallery/create.html', context)


@login_required(login_url=settings.LOGIN_URL)
def delete(request, pk):
    gallery = get_object_or_404(Gallery, pk=pk)
    gallery.delete()
    messages.success(request, 'Data Berhasil Dihapus')

    return redirect('/gallery/')