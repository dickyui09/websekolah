from django.shortcuts import render, redirect, HttpResponseRedirect
from django.contrib.auth import authenticate, logout, login
from django.contrib import messages

from ..models import Login
from ..forms import LoginForms
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required


def views(request):
    if request.session.has_key('username'):
      username = request.session['username']

      context = {
          'username' : username
      }
      return render(request, 'admin/dashboard/index.html', context)
      
    else:
      return render(request, 'admin/auth/login.html')