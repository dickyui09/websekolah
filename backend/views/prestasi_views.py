from django.shortcuts import render, redirect, get_object_or_404
from ..forms import PrestasiForms
from ..models import Prestasi
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from rest_framework import viewsets
from ..serializers import PrestasiSerializer

from django.conf import settings

class PrestasiList(viewsets.ModelViewSet):
    queryset = Prestasi.objects.all().order_by('name')
    serializer_class = PrestasiSerializer

    def get_options(self):
        return "options", {
            "prestasi": [{'label': obj.name, 'value': obj.pk} for obj in Prestasi.objects.all()],            
        }

    class Meta:
        datatables_extra_json = ('get_options', )

@login_required(login_url=settings.LOGIN_URL)
def index(request):
    context = {
        'title' : 'Prestasi',
        'prestasi_status' : 'active',
        'category_status' : 'active',
    }
    return render(request, 'admin/Category/prestasi/list.html', context)

@login_required(login_url=settings.LOGIN_URL)
def create(request):
    form = PrestasiForms(request.POST or None)
    if request.method == 'POST':
        if request.POST and form.is_valid():
            prestasi = form.save(commit=False)
            prestasi.save()
            messages.success(request, 'Data Telah Tersimpan')
            return redirect('/prestasi/')              

            form = PrestasiForms()
        else:
            messages.warning(request, 'Data Gagal Disimpan')
            return redirect('backend_prestasi_create')             

    context = {
        'title' : 'Tambah Prestasi',
        'prestasi_status' : 'active',
        'category_status' : 'active',        
        'form' : form,
    }
    return render(request, 'admin/Category/prestasi/create.html', context)

@login_required(login_url=settings.LOGIN_URL)
def edit(request, pk):        
    instance = get_object_or_404(Prestasi, pk=pk)
    form = PrestasiForms(request.POST or None, request.FILES or None,  instance=instance)
    if request.method == 'POST':        
        if request.POST and form.is_valid():
            form.save()
            messages.success(request, 'Data Berhasil Diubah')
            return redirect('/prestasi/')
        else:
            form = PrestasiForms(instance=instance)
            messages.warning(request, 'Data Gagal Diubah')

    context = {
        'title' : 'Edit Prestasi',        
        'prestasi_status' : 'active',
        'category_status' : 'active',   
        'form': form,     
    }

    return render(request, 'admin/Category/prestasi/create.html', context)

@login_required(login_url=settings.LOGIN_URL)
def delete(request, pk):
    prestasi = get_object_or_404(Prestasi, pk=pk)
    prestasi.delete()
    messages.success(request, 'Data Berhasil Dihapus')

    return redirect('/prestasi/')