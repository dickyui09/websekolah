from django.urls import path, include
from rest_framework import routers
from django.conf import settings
from django.conf.urls.static import static

from .views import auth_views, fasilitas_views, ekskul_views, prestasi_views, dashboard_views, events_views, comments_views, gallery_views, profile_views, conn_views
from django.contrib.auth.decorators import login_required

router = routers.DefaultRouter()
router.register('ekskul', ekskul_views.EkskulList)
router.register('fasilitas', fasilitas_views.FasilitasList)
router.register('prestasi', prestasi_views.PrestasiList)
router.register('events', events_views.EventsList)
router.register('profile', profile_views.ProfileList)
router.register('gallery', gallery_views.GalleryList)
router.register('comment', comments_views.CommentsList)

app_name = 'backend'
urlpatterns = [
    path('api/', include(router.urls)),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('', dashboard_views.index_profile, name="backend_dashboard_profile"),
    path('conn/', conn_views.views, name="backend_conn"),
    
    path('admin/', auth_views.login_view, name="backend_auth_login"),
    path('logout/', auth_views.logout_view, name="backend_auth_logout"),

    path('dashboard/', dashboard_views.index, name="backend_dashboard"),
    path('ekskul/', ekskul_views.index, name="backend_ekskul"),
    path('ekskul/profile/', ekskul_views.index_profile, name="backend_ekskul_profile"),
    path('ekskul/create/', ekskul_views.create, name="backend_ekskul_create"),
    path('ekskul/edit/<uuid:pk>', ekskul_views.edit, name="backend_ekskul_edit"),    
    path('ekskul/delete/<uuid:pk>', ekskul_views.delete, name="backend_ekskul_delete"),

    path('fasilitas/', fasilitas_views.index, name="backend_fasilitas"),
    path('fasilitas/profile/', fasilitas_views.index_profile, name="backend_fasilitas_profile"),
    path('fasilitas/create/', fasilitas_views.create, name="backend_fasilitas_create"),
    path('fasilitas/edit/<uuid:pk>', fasilitas_views.edit, name="backend_fasilitas_edit"),    
    path('fasilitas/delete/<uuid:pk>', fasilitas_views.delete, name="backend_fasilitas_delete"),

    path('prestasi/', prestasi_views.index, name="backend_prestasi"),
    path('prestasi/create/', prestasi_views.create, name="backend_prestasi_create"),
    path('prestasi/edit/<uuid:pk>', prestasi_views.edit, name="backend_prestasi_edit"),    
    path('prestasi/delete/<uuid:pk>', prestasi_views.delete, name="backend_prestasi_delete"),

    path('events/', events_views.index, name="backend_events"),
    path('events/create/', events_views.create, name="backend_events_create"),
    path('events/detail/<uuid:pk>', events_views.detail_events, name="backend_events_detail"),
    # path('events/detail/<uuid:pk>/page', events_views.detail_page, name="backend_events_page"),
    path('events/edit/<uuid:pk>/', events_views.edit, name="backend_events_edit"),    
    path('events/delete/<uuid:pk>', events_views.delete, name="backend_events_delete"),

    path('profile/', profile_views.index, name="backend_profile"),
    path('profile/create/', profile_views.create, name="backend_profile_create"),
    path('profile/edit/<uuid:pk>', profile_views.edit, name="backend_profile_edit"),    
    # path('profile/delete/<uuid:pk>', profile_views.delete, name="backend_profile_delete"),

    path('gallery/', gallery_views.index, name="backend_gallery"),
    path('gallery/create/', gallery_views.create, name="backend_gallery_create"),   
    path('gallery/edit/<uuid:pk>', gallery_views.edit, name="backend_gallery_edit"),    
    path('gallery/delete/<uuid:pk>', gallery_views.delete, name="backend_gallery_delete"), 

    path('comments/', comments_views.index, name="backend_comments"),    
    path('comments/delete/<uuid:pk>', comments_views.delete, name="backend_comments_delete"), 
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


    