from rest_framework import serializers
from .models import Ekskul, Fasilitas, Prestasi, Events, Profile, Gallery, Comments


class EkskulSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ekskul
        fields = (
            'id', 'name', 'image'
        )
        datatables_always_serialize = ('id', 'name', 'image', )

class FasilitasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fasilitas
        fields = (
            'id', 'name', 'image', 'jumlah'
        )
        datatables_always_serialize = ('id', 'name', 'image', 'jumlah', )

class PrestasiSerializer(serializers.ModelSerializer):
    class Meta:
        model = Prestasi
        fields = (
            'id', 'name', 'juara', 'peraih'
        )
        datatables_always_serialize = ('id', 'name', 'juara', 'peraih', )

class EventsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Events
        fields = (
            'id', 'judul', 'image', 'deskripsi'
        )
        datatables_always_serialize = ('id', 'judul', 'image', 'deskripsi', )

class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = (
            'id', 'name', 'alamat', 'contact', 'image'
        )
        datatables_always_serialize = ('id', 'name', 'alamat', 'contact', 'image',  )

class GallerySerializer(serializers.ModelSerializer):
    class Meta:
        model = Gallery
        fields = (
            'id', 'deskripsi', 'image'
        )
        datatables_always_serialize = ('id', 'deskripsi', 'image', )

class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comments
        fields = (
            'id', 'email', 'comment'
        )
        datatables_always_serialize = ('id', 'email', 'comment', )