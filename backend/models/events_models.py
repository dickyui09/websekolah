import uuid
from django.db import models
from django.urls import reverse
# from sorl.thumbnail import ImageField, get_thumbnail


class Events(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    judul = models.CharField(max_length=50)
    deskripsi = models.TextField()
    image = models.ImageField(upload_to='images/')
    slug = models.SlugField(null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse('backend:backend_events_detail', kwargs={"pk": self.pk})
        return reverse('backend:backend_events_detail', kwargs={'slug': self.slug})

    # def save(self):
    #     if self.image:
    #         self.image = get_thumbnail(self.image, '500x600', quality=99, format='JPEG')
    #     super(Events, self).save()
