from .auth_models import Login
from .ekskul_models import Ekskul
from .fasilitas_models import Fasilitas
from .prestasi_models import Prestasi
from .events_models import Events
from .profile_models import Profile
from .gallery_models import Gallery
from .comments_models import Comments