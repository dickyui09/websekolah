import uuid
from django.db import models
from django.urls import reverse
# from ckeditor.fields import RichTextField

class Ekskul(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=50)    
    image = models.ImageField(upload_to='images/')
    status = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return str(self.id)

