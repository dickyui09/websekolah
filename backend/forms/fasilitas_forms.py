from django import forms
from ..models import Fasilitas

class FasilitasForms(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(FasilitasForms, self).__init__(*args, **kwargs)
        self.fields['image'].required = False
    class Meta():
        model = Fasilitas
        fields = [
             'name', 'image', 'jumlah'
            ]
        widgets = {            
            'name': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'name',
                }
            ),
            'image': forms.FileInput(
                attrs={
                    'class': 'custom-file-input',
                    'id': 'customFile',
                }
            ),
            'jumlah': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': '0',
                }
            ),
        }

