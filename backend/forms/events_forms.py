from django import forms
from ..models import Events

class EventsForms(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(EventsForms, self).__init__(*args, **kwargs)
        self.fields['image'].required = False
    class Meta():
        model = Events
        fields = [
             'judul', 'deskripsi', 'image'
            ]
        widgets = {            
            'judul': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'name',
                }
            ),
            'deskripsi': forms.Textarea(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Deskripsi',
                    "rows":20,
                    "cols":20,
                }
            ),
            'image': forms.FileInput(
                attrs={
                    'class': 'custom-file-input',
                    'id': 'customFile',
                }
            ),
        }

