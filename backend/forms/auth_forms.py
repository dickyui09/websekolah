from django import forms
from django.contrib.auth import login, authenticate
from ..models import Login

class LoginForms(forms.Form):
	username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control','placeholder': 'Username'}), required=True)
	password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control','placeholder': 'password'}), required=True)	

	def __init__(self, *args, **kwargs):
		super(LoginForms, self).__init__(*args, **kwargs)
		self.fields['username'].widget.attrs.update({
		    'class': 'form-control',
		    "name":"username"})
		self.fields['password'].widget.attrs.update({
		    'class': 'form-control',
		    "name":"password"})

	def clean(self, *args, **keyargs):
		username = self.cleaned_data.get("username")
		password = self.cleaned_data.get("password")

		if username and password:
			user = authenticate(username = username, password = password)
			if not user:
				raise forms.ValidationError("This user does not exists")
			if not user.check_password(password):
				raise forms.ValidationError("Incorrect Password")
			if not user.is_active:
				raise forms.ValidationError("User is no longer active")

		return super(LoginForms, self).clean(*args, **keyargs)

# class LoginForms(forms.ModelForm):    
#     class Meta():
#         model = Login
#         fields = [
#              'username', 'password'
#             ]
#         widgets = {            
#             'username': forms.TextInput(
#                 attrs={
#                     'class': 'form-control',
#                     'placeholder': 'Username',
#                 }
#             ),
#             'password': forms.PasswordInput(
#                 attrs={
#                     'class': 'form-control',
#                     'placeholder': 'Password',
#                 }
#             ),

#         }

#     def m_login(self, request):
#         username = self.cleaned_data.get('username')
#         password = self.cleaned_data.get('password')
#         auth = authenticate(username=username, password=password)
#         login(request, auth)
#         return auth