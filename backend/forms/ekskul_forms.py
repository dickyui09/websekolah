from django import forms
from ..models import Ekskul

class EkskulForms(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(EkskulForms, self).__init__(*args, **kwargs)
        self.fields['image'].required = False
    class Meta():
        model = Ekskul
        fields = [
             'name', 'image'
            ]
        widgets = {            
            'name': forms.TextInput(
                attrs={
                    'class' : 'form-control',
                    'placeholder': 'Nama Ektrakulikuler',
                }
            ),
            'image': forms.FileInput(
                attrs={
                    'class': 'custom-file-input',
                    'id': 'customFile',
                }
            ),
        }

