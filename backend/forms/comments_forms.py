from django import forms
from ..models import Comments

class CommentsForms(forms.ModelForm):
    class Meta():
        model = Comments
        fields = [
             'name', 'email', 'comment'
            ]
        widgets = { 
            'name': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Nama Lengkap Anda',
                }
            ),           
            'email': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Email',
                    'type' : 'email',
                }
            ),
            'comment': forms.Textarea(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Komentar',
                    'rows':'10',
                }
            ),

        }

