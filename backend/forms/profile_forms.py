from django import forms
from ..models import Profile

class ProfileForms(forms.ModelForm):
    class Meta():
        model = Profile
        fields = [
             'name', 'alamat', 'image', 'contact'
            ]
        widgets = {            
            'name': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Nama Sekolah',
                }
            ),            
            'alamat': forms.Textarea(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'alamat',
                    "rows":20,
                    "cols":20,
                }
            ),   
            'image': forms.FileInput(
                attrs={
                    'class': 'custom-file-input',
                    'id': 'customFile',
                }
            ),        
            'contact': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'contact',
                }
            ),            
        }

