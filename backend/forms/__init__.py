from .auth_forms import LoginForms
from .ekskul_forms import EkskulForms
from .fasilitas_forms import FasilitasForms
from .prestasi_forms import PrestasiForms
from .events_forms import EventsForms
from .profile_forms import ProfileForms
from .gallery_forms import GalleryForms
from .comments_forms import CommentsForms