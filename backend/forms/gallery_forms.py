from django import forms
from ..models import Gallery

class GalleryForms(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(GalleryForms, self).__init__(*args, **kwargs)
        self.fields['image'].required = False

    class Meta():
        model = Gallery
        fields = [
             'deskripsi', 'image'
            ]
        widgets = {            
            'deskripsi': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'name',
                }
            ),
            'image': forms.FileInput(
                attrs={
                    'class': 'custom-file-input',
                    'id': 'customFile',
                }
            ),
        }

