from django import forms
from ..models import Prestasi

class PrestasiForms(forms.ModelForm):
    class Meta():
        model = Prestasi
        fields = [
             'name', 'juara', 'peraih'
            ]
        widgets = {            
            'name': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'name',
                }
            ),
            'juara': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'status',
                }
            ),
            'peraih': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'peraih',
                }
            ),
        }

