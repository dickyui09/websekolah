$(function () {
    'use strict';
    var data_ajax = eval($('.ajax_data').data('value'));
    var fix_data = null;
    if (data_ajax) {
        fix_data = data_ajax[0];
    }
    var oTable = $('#datatable1').DataTable({
        responsive: true,
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        },
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
        },
        processing: true,
        serverSide: true,
        ajax: {
            url: $('.ajax_url').data('url'),
            data: fix_data
        },
        "columnDefs": [{
            "orderable": false,
            "targets": [0, $('.target_action').data('target')]
        }]
    })
    // Select2
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity
    });

    var form = $('.filter-form');
    $('.filter-form select').on('change', function () {
        form.submit();
    });

    form.on('submit', function (e) {
        e.preventDefault();

        var formData = $(this).serializeArray();
        var qs = [];
        formData.forEach(fd => {
            fd.value != 'all' ? qs.push(`${fd.name}=${fd.value}`) : false;
        });

        var url = $('.ajax_url').data('url');
        url += qs.length > 0 ? '?' + qs.join('&') : '';
        oTable.ajax.url(url).load();
    });

});