$(document).ready(function() {
    if ($.fn.dataTable) {
        $.extend(true, $.fn.dataTable.defaults, {
            "oLanguage": {
                "sEmptyTable":   "Tidak ada data yang tersedia pada tabel ini",
                "sProcessing":   "Sedang memproses...",
                "sLengthMenu":   "Tampilkan _MENU_ entri",
                "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix":  "",
                "sSearch":       "",
                "sSearchPlaceholder": "Cari...",
                "sUrl":          "",
                "oPaginate": {
                    "sFirst":    "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext":     "Selanjutnya",
                    "sLast":     "Terakhir"
                }
            }
        });

        if ($.fn.editableform) {
            $.fn.editableform.template = '<form class="form-horizontal editableform">'+
                                    '<div class="control-group">'+
                                        '<div><div class="editable-input"></div><div class="editable-buttons"></div></div>'+
                                        '<div class="editable-error-block text-danger"></div>'+
                                    '</div>'+ 
                                '</form>';
        }
        
        var datatablesClassName = ".datatable-class";
        
        if ($(datatablesClassName).length > 0) {
            $(datatablesClassName).each(function() {
                var dt = new Datatables(this);
                dt.initialize();
            });
        }

        function initDatatablesEditable(dt, token) {
            if (!$.fn.editableform) {
                return false;
            }

            $('.editable', dt.api().table().body()).editable({
                params: {
                    csrfmiddlewaretoken: token
                },
                error: function(response, newValue) {
                    if(response.status === 500) {
                        return 'insert number!';
                    } else {
                        return response.responseText;
                    }
                }
            }).off('hidden').on('hidden', function(e, reason) {
                if(reason === 'save') {
                    $(dt).closest('td span').attr('data-order', $(dt).text());
                    dt.api().ajax.reload()
                }
            });
        }
        
        function Datatables(element) {
            this.element = element;
            this.defOptions = {
                responsive: true,
                processing: true,
                serverSide: true,
            };
            this.initialize = function() {
                var options = $(this.element).data();

                if ('columnIndex' in options && options.columnIndex == 1) {
                    options['fnRowCallback'] = function (nRow, aData, iDisplayIndex) {
                        var info = $(this).DataTable().page.info();
                        $("td:nth-child(1)", nRow).html(info.start + iDisplayIndex + 1);
                        return nRow;
                    };

                    var oci = {
                        targets: [0],
                        orderable: false,
                    };

                    if ('columnDefs' in options) {
                        options.columnDefs = eval(options.columnDefs)
                        options.columnDefs.push(oci);
                    } else {
                        options.columnDefs = [oci];
                    }

                    // remove options
                    delete options.columnIndex;
                }

                options['drawCallback'] = function(settings){
                    initDatatablesEditable(this, $('.csrf_token').val());
                };

                if ('columnAction' in options) {
                    var oca = {
                        targets: [options.columnAction],
                        className: 'text-right pr-4',
                        orderable: false
                    };

                    if ('columnDefs' in options) {
                        options.columnDefs = eval(options.columnDefs)
                        options.columnDefs.push(oca);
                    } else {
                        options.columnDefs = [oca];
                    }

                    delete options.columnAction;
                }
                
                if ('columnFoot' in options && $(this.element).find('tfoot')) {
                    
                    options['footerCallback'] = function ( row, data, start, end, display ) {
                        var _options = Object.assign({}, $(this[0]).data());
                        var api = this.api(), data;
                        var intVal = function ( i ) {
                            return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                            i : 0;
                        };

                        // console.log(_options);
                        
                        try {
                            _options['columnFoot'].forEach(k => {
                                var total = api.column(k).data().reduce( function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);
                                
                                var footElement = $(this[0]).find(`tfoot th:eq(${k})`);
                                var render = eval(footElement.data('render'));
                                total = render(total);
                                
                                $(api.column(k).footer()).html(total);
                            });
                        } catch(err) {}
                    }

                    // remove options
                    // delete options.columnFoot;
                }
                
                Object.keys(options).forEach(k => {
                    try {
                        options[k] = eval(options[k]);
                    } catch(err){}
                });
                
                this.options = Object.assign({}, this.defOptions, options);
                this.render();
            }
            this.render = function() {
                var options = $(this.element).data();
                
                // initialize datatables
                var dt = $(this.element).DataTable(this.options);
                
                // initialize select2
                $(this.element).parents('.dataTables_wrapper').find('.dataTables_length select').select2({
                    minimumResultsForSearch: Infinity
                });
                
                // filter form
                if ('filterForm' in options && $(options.filterForm).length === 1) {
                    $(options.filterForm).on('submit', function(e) {
                        e.preventDefault();
                        
                        var formData = $(this).serializeArray();
                        var qs = [];
                        formData.forEach(fd => {
                            fd.value != 'all' ? qs.push(`${fd.name}=${fd.value}`) : false;
                        });
                        
                        var glue = options.ajax.includes('?') ? '&' : '?';
                        var url = options.ajax;
                        url += qs.length > 0 ? glue + qs.join('&') : '';
                        dt.ajax.url(url).load();
                    });
                }
            }
        }
    }
});
